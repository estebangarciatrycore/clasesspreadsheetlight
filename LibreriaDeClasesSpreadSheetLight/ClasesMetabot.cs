﻿using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using System;
using System.IO;

namespace LibreriaDeClasesSpreadSheetLight
{
    public class ClasesMetabot
    {
        public static string ConvertirExcelACsv(string strEjecucion,string strRutaExcel, string strNombreArchivoExcel, string strRutaCsv, string strNombreArchivoCsv)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "usuariomiExcel.xlsx";
            //string strRutaCsv = "C:\\Users\\usuario\\Desktop";
            //string strNombreArchivoCsv = "WriteLines.csv";

            string strEstadoConversion = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1")) {

                try
                {
                    // Crea una isntancia de tipo SLDocument con el archivo de excel seleccionado con ruta y nombre

                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);

                    // Inicializa contadores y acumuladores

                    int intColumn = 1;
                    int intFila = 1;
                    string strContenidoFilaCompleta = "";
                    string strContenidoFilaSiguiente = "B";

                    // Valida cuantas columnas tiene el archivo contando los titulos encabezados 
                    while ((!string.IsNullOrEmpty(oSLDocument.GetCellValueAsString(1, intColumn))) && (!string.IsNullOrEmpty(oSLDocument.GetCellValueAsString(1, intColumn + 1))))
                    {
                        strContenidoFilaCompleta = strContenidoFilaCompleta + oSLDocument.GetCellValueAsString(1, intColumn);
                        intColumn++;
                    }

                    // Valida que el archivo contenga encabezados 

                    if (!string.IsNullOrEmpty(strContenidoFilaCompleta))
                    {

                        do
                        {
                            //Inicializa las variables que utiliza en el ciclo de extracción por fila

                            string strContenidoFila = "";
                            strContenidoFilaCompleta = "";
                            strContenidoFilaSiguiente = "";

                            //Agrupa en una linea de texto las columnas de n fila y las delimita por coma

                            for (int i = 1; i <= intColumn; i++)
                            {

                                if (i == 1)
                                {
                                    strContenidoFila = oSLDocument.GetCellValueAsString(intFila, i);
                                }
                                else
                                {
                                    strContenidoFila = strContenidoFila + "," + oSLDocument.GetCellValueAsString(intFila, i);
                                }
                                strContenidoFilaCompleta = strContenidoFilaCompleta + oSLDocument.GetCellValueAsString(intFila, i);
                                strContenidoFilaSiguiente = strContenidoFilaSiguiente + oSLDocument.GetCellValueAsString(intFila + 1, i);
                            }

                            //inicia el archivo .csv en limpio

                            if (intFila.Equals(1))
                            {
                                File.WriteAllText(strRutaCsv + "\\" + strNombreArchivoCsv, String.Empty);
                            }

                            //Añade la linea de texto a un archivo .csv

                            using (StreamWriter outputFile = new StreamWriter(Path.Combine(strRutaCsv, strNombreArchivoCsv), true))
                            {
                                outputFile.WriteLine(strContenidoFila);
                            }

                            //Aumenta la fila

                            intFila++;

                        } while ((!string.IsNullOrEmpty(strContenidoFilaCompleta)) && (!string.IsNullOrEmpty(strContenidoFilaSiguiente)));

                    }
                    strEstadoConversion = "Exitoso";
                }
                catch(Exception e) {
                    strEstadoConversion = "No exitoso, debido a: " + e;
                }
            }

            return strEstadoConversion;
        }
        public static string CrearExcel(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoEliminacion = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument();

                    //guarda esa instancia como un archivo .xlsx
                    oSLDocument.SaveAs(strRutaExcel + "\\" + strNombreArchivoExcel);

                    strEstadoEliminacion = "Exitoso";
                }
                catch (Exception e)
                {
                    strEstadoEliminacion = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoEliminacion;
        }
        public static string EliminarExcel(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoCreacion = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1")) {

                try {

                    if (File.Exists(strRutaExcel + "\\" + strNombreArchivoExcel)) {
                        File.Delete(strRutaExcel + "\\" + strNombreArchivoExcel);
                    }
                    
                    strEstadoCreacion = "Exitoso";
                }
                catch (Exception e)
                {
                    strEstadoCreacion = "No exitoso, debido a: " + e;
                }

            }
            
            return strEstadoCreacion;
        }
        public static string AsignarCelda(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel,string strNombreHoja,int intIndexRow,int intIndexColumn,string strValor)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoAsignacion = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {
                
                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHoja);
                    oSLDocument.SetCellValue(intIndexRow,intIndexColumn,strValor);
                    oSLDocument.Save();
                    strEstadoAsignacion = "Exitoso";

                }
                catch (Exception e)
                {
                    strEstadoAsignacion = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoAsignacion;
        }
        public static (string,string) ObtenerCelda(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja, int intIndexRow, int intIndexColumn)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoObtencion = "No exitoso";
            string strValorCelda = "";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHoja);
                    strValorCelda=oSLDocument.GetCellValueAsString(intIndexRow,intIndexColumn);

                    strEstadoObtencion = "Exitoso";
                }
                catch (Exception e)
                {
                    strEstadoObtencion = "No exitoso, debido a: " + e;
                }

            }

            return (strEstadoObtencion,strValorCelda);
        }
        public static string LimpiarCeldas(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja, int intStartIndexRow, int intStartIndexColumn, int intEndIndexRow, int intEndIndexColumn)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoAsignacion = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHoja);
                    oSLDocument.ClearCellContent(intStartIndexRow,intStartIndexColumn,intEndIndexRow,intEndIndexColumn);
                    oSLDocument.Save();
                    strEstadoAsignacion = "Exitoso";

                }
                catch (Exception e)
                {
                    strEstadoAsignacion = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoAsignacion;
        }
        public static string CopiarCeldas(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja, int intStartIndexRow, int intStartIndexColumn, int intEndIndexRow, int intEndIndexColumn,int intIndexRowsDest,int intIndexColumnDest)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoCopiado = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHoja);
                    oSLDocument.CopyCell(intStartIndexRow,intStartIndexColumn,intEndIndexRow,intEndIndexColumn,intIndexRowsDest,intIndexColumnDest);
                    oSLDocument.Save();
                    strEstadoCopiado = "Exitoso";

                }
                catch (Exception e)
                {
                    strEstadoCopiado = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoCopiado;
        }
        public static string CopiarCeldasEntreHojas(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHojaActual,string strNombreHojaDestino, int intStartIndexRow, int intStartIndexColumn, int intEndIndexRow, int intEndIndexColumn, int intIndexRowsDest, int intIndexColumnDest)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoCopiado = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHojaDestino);
                    oSLDocument.CopyCellFromWorksheet(strNombreHojaActual,intStartIndexRow, intStartIndexColumn, intEndIndexRow, intEndIndexColumn, intIndexRowsDest, intIndexColumnDest);
                    oSLDocument.Save();
                    strEstadoCopiado = "Exitoso";

                }
                catch (Exception e)
                {
                    strEstadoCopiado = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoCopiado;
        }
        public static string AsignarColorRellenoCeldas(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja, int intStartIndexRow, int intStartIndexColumn, int intEndIndexRow, int intEndIndexColumn, int intRojo, int intVerde,int intAzul)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoCopiado = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea un Estilo
                    SLStyle sLStyle = new SLStyle();
                    sLStyle.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.FromArgb(intRojo, intVerde, intAzul), System.Drawing.Color.FromArgb(intRojo, intVerde, intAzul));

                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHoja);
                    oSLDocument.SetCellStyle(intStartIndexRow, intStartIndexColumn, intEndIndexRow, intEndIndexColumn, sLStyle);
                    oSLDocument.Save();
                    strEstadoCopiado = "Exitoso";

                }
                catch (Exception e)
                {
                    strEstadoCopiado = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoCopiado;
        }
        public static string AsignarColorTextoCeldas(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja, int intStartIndexRow, int intStartIndexColumn, int intEndIndexRow, int intEndIndexColumn, int intRojo, int intVerde, int intAzul)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoCopiado = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea un Estilo
                    SLStyle sLStyle = new SLStyle();
                    sLStyle.SetFontColor(System.Drawing.Color.FromArgb(intRojo, intVerde, intAzul));

                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHoja);
                    oSLDocument.SetCellStyle(intStartIndexRow, intStartIndexColumn, intEndIndexRow, intEndIndexColumn, sLStyle);
                    oSLDocument.Save();
                    strEstadoCopiado = "Exitoso";

                }
                catch (Exception e)
                {
                    strEstadoCopiado = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoCopiado;
        }
        public static string CrearHoja(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoCreacionHoja = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.AddWorksheet(strNombreHoja);
                    oSLDocument.Save();

                    strEstadoCreacionHoja = "Exitoso";
                }
                catch (Exception e)
                {
                    strEstadoCreacionHoja = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoCreacionHoja;
        }
        public static string EliminarHoja(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoEliminarHoja = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.DeleteWorksheet(strNombreHoja);
                    oSLDocument.Save();

                    strEstadoEliminarHoja = "Exitoso";
                }
                catch (Exception e)
                {
                    strEstadoEliminarHoja = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoEliminarHoja;
        }
        public static string MoverHoja(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja, int intPosicion)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoMoverHoja = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.MoveWorksheet(strNombreHoja,intPosicion);
                    oSLDocument.Save();

                    strEstadoMoverHoja = "Exitoso";
                }
                catch (Exception e)
                {
                    strEstadoMoverHoja = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoMoverHoja;
        }
        public static string RenombrarHoja(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHojaActual, string strNombreHojaNuevo)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoRenombrarHoja = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.RenameWorksheet(strNombreHojaActual,strNombreHojaNuevo);
                    oSLDocument.Save();

                    strEstadoRenombrarHoja = "Exitoso";
                }
                catch (Exception e)
                {
                    strEstadoRenombrarHoja = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoRenombrarHoja;
        }
        public static string LimpiarHoja(string strEjecucion, string strRutaExcel, string strNombreArchivoExcel, string strNombreHoja)
        {
            //string strRutaExcel = "C:\\Users\\usuario\\Desktop";
            //string strNomreArchivoExcel = "pruebacreación.xlsx";
            string strEstadoAsignacion = "No exitoso";

            //Valida si esta función de clase fue seleccionada para ejecutarse

            if (strEjecucion.Equals("1"))
            {

                try
                {
                    // Crea una isntancia de tipo SLDocument
                    SLDocument oSLDocument = new SLDocument(strRutaExcel + "\\" + strNombreArchivoExcel);
                    oSLDocument.SelectWorksheet(strNombreHoja);
                    oSLDocument.ClearCellContent();
                    oSLDocument.Save();
                    strEstadoAsignacion = "Exitoso";

                }
                catch (Exception e)
                {
                    strEstadoAsignacion = "No exitoso, debido a: " + e;
                }

            }

            return strEstadoAsignacion;
        }


    }
}
